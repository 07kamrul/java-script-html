﻿using System;

namespace Pattern_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pattern 1");
            Console.Write("No Of Row : ");
            int row, i, j;
            row = Convert.ToInt32(Console.ReadLine());
            for (i = 1; i <= row; i++)
            {
                for (j = 1; j <= row; j++)
                {
                    Console.Write(" * ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
