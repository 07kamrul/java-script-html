﻿using System;

namespace pattern_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int row, i, j;
            Console.WriteLine("Pattern_2");
            Console.Write("No OF Row : ");
            row = Convert.ToInt32(Console.ReadLine());
            for (i = 1; i <= row; i++ )
            {
                for (j = 1; j <= i; j++)
                {
                    Console.Write(" * ");
                }
                Console.WriteLine();

            }
            Console.ReadLine();
        }
    }
}
